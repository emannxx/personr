import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PublicationNewComponent} from './publication-new.component';

const routes: Routes = [
  {
      path: '',
      component: PublicationNewComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PublicationNewRoutingModule { }
