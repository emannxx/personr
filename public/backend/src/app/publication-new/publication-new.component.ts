import { Component, OnInit } from '@angular/core';
import { PublicationsService } from '../shared/services/publications.api.service';
import { Publication } from '../models/publication.model';

@Component({
  selector: 'app-publication-new',
  templateUrl: './publication-new.component.html',
  styleUrls: ['./publication-new.component.scss']
})
export class PublicationNewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
