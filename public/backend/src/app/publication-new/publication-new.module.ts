import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PublicationNewRoutingModule} from './publication-new-routing.module';
import {PublicationNewComponent} from './publication-new.component';
import {MaterialComponentsModule} from '../layout/material-components/material-components.module';


@NgModule({
  imports: [
    CommonModule,
    PublicationNewRoutingModule,
    MaterialComponentsModule
  ],
  declarations: [PublicationNewComponent]
})
export class PublicationNewModule { }
