import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicationDetailRoutingModule } from './publication-detail-routing.module';
import { PublicationDetailComponent } from './publication-detail.component';



@NgModule({
  imports: [
    CommonModule,
    PublicationDetailRoutingModule
  ],
  declarations: [PublicationDetailComponent]
})
export class PublicationDetailModule { }
