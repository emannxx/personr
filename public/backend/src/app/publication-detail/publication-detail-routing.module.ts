import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicationDetailComponent } from './publication-detail.component';

const routes: Routes = [
  {
      path: '',
      component: PublicationDetailComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PublicationDetailRoutingModule { }
