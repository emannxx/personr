import { Component, OnInit } from '@angular/core';
import { PublicationsService } from '../shared/services/publications.api.service';
import {ActivatedRoute} from '@angular/router';
import { Publication } from '../models/publication.model';

@Component({
  selector: 'app-publication-detail',
  templateUrl: './publication-detail.component.html',
  styleUrls: ['./publication-detail.component.scss']
})
export class PublicationDetailComponent implements OnInit {
  publicationLoaded: Publication;
  pubId:number;
  constructor(private route:ActivatedRoute, private publicationService: PublicationsService) { }

  ngOnInit(){
    this.route.params.subscribe( params => this.pubId = params['id']);
    
    this.publicationService.getPublicationById(this.pubId).subscribe(
      data => (this.publicationLoaded = data,console.log(data)),
      //@ts-ignore
      error => (emitNotification(error.message),console.log(error))
    );
  }
}
