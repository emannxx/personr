import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guard/auth.guard';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'charts',
                loadChildren: () => import('./charts/charts.module').then(m => m.ChartsModule)
            },
            {
                path: 'components',
                loadChildren:
                    () => import('./material-components/material-components.module').then(m => m.MaterialComponentsModule)
            },
            {
                path: 'forms',
                loadChildren: () => import('./forms/forms.module').then(m => m.FormsModule)
            },
            {
                path: 'grid',
                loadChildren: () => import('./grid/grid.module').then(m => m.GridModule)
            },
            {
                path: 'tables',
                loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule)
            },
            {
                path: 'blank-page',
                loadChildren: () => import('./blank-page/blank-page.module').then(m => m.BlankPageModule)
            },
            {
                path: 'publications',
                loadChildren: () => import('../publications/publications.module').then(m => m.PublicationsModule),
                canActivate: [AuthGuard]
            },
            {
                path: 'publication/:id',
                loadChildren: () => import('../publication-detail/publication-detail.module').then(m => m.PublicationDetailModule),
                canActivate: [AuthGuard]
            },
            {
                path: 'publication-new',
                loadChildren: () => import('../publication-new/publication-new.module').then(m => m.PublicationNewModule),
                canActivate: [AuthGuard]
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [AuthGuard]
})
export class LayoutRoutingModule {}
