import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

import {
    BottomSheetOverviewComponent
} from './bottom-sheet-overview/bottom-sheet-overview.component';
import { BottomSheetComponent } from './bottom-sheet/bottom-sheets.component';
import { ButtonComponent } from './button/button.component';
import { DialogOverviewComponent } from './dialog-overview/dialog-overview.component';
import { DialogComponent } from './dialog/dialog.component';
import { MaterialComponentsRoutingModule } from './material-components-routing.module';
import { MaterialComponentsComponent } from './material-components.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { PizzaPartyComponent, SnackBarComponent } from './snack-bar/snack-bar.component';
import { ToolTipComponent } from './tool-tip/tool-tip.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialComponentsRoutingModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatButtonModule,
        MatListModule,
        MatBottomSheetModule,
        MatSnackBarModule,
        MatCardModule,
        ReactiveFormsModule,
        MatOptionModule,
        MatSelectModule,
        MatTooltipModule,
        MatRadioModule,
        MatSliderModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatIconModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    exports: [
        CommonModule,
        MaterialComponentsRoutingModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatButtonModule,
        MatListModule,
        MatBottomSheetModule,
        MatSnackBarModule,
        MatCardModule,
        ReactiveFormsModule,
        MatOptionModule,
        MatSelectModule,
        MatTooltipModule,
        MatRadioModule,
        MatSliderModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatIconModule
    ],
    declarations: [
        MaterialComponentsComponent,
        DialogComponent,
        DialogOverviewComponent,
        BottomSheetComponent,
        BottomSheetOverviewComponent,
        SnackBarComponent,
        PizzaPartyComponent,
        ToolTipComponent,
        PaginatorComponent,
        ProgressBarComponent,
        ButtonComponent
    ],
    entryComponents: [
        DialogOverviewComponent,
        BottomSheetOverviewComponent,
        PizzaPartyComponent
    ]
})
export class MaterialComponentsModule {}
