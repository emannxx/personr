import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Publication } from '../../models/publication.model';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PublicationsService {
  apiURL: string = '/api';
  public firstPage: string = "";
  public prevPage: string = "";
  public nextPage: string = "";
  public lastPage: string = "";

  constructor(private httpClient: HttpClient) {}

  public createPublication(publication: Publication){
    return this.httpClient.post(`${this.apiURL}/publications/`,publication);
  }

  public updatePublication(publication: Publication){
    return this.httpClient.put(`${this.apiURL}/publications/${publication.id}`,publication);
  }

  public deletePublication(id: number){
    return this.httpClient.delete(`${this.apiURL}/publications/${id}`);
  }

  public getPublicationById(id: number){
    return this.httpClient.get<Publication>(`${this.apiURL}/publications/${id}`);
  }

  public getPublications(url?: string){
    if(url){
      return this.httpClient.get<Publication[]>(`${this.apiURL}/publications${url}`,{ observe: 'response' }).pipe(tap(res => {
        this.retrieve_pagination_links(res);
      }));
    }

    return this.httpClient.get<Publication[]>(`${this.apiURL}/publications?page=1`,
    { observe: 'response' }).pipe(tap(res => {
      this.retrieve_pagination_links(res); 
    }));
  }

  public retrieve_pagination_links(response){
    var regex = /([^\?]+)(\?.*)?/g;
    var fpg = regex.exec(response.body.first_page_url);
    var lpg = regex.exec(response.body.last_page_url);
    var npg = regex.exec(response.body.next_page_url);
    var ppg = regex.exec(response.body.prev_page_url);
    
    this.firstPage  = (fpg != null) ? fpg[2] : null;
    this.lastPage =  (lpg != null) ? lpg[2] : null;
    this.nextPage =  (npg != null) ? npg[2] : null;
    this.prevPage =  (ppg != null) ? ppg[2] : null;
  }
}
