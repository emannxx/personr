import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicationsRoutingModule } from './publications-routing.module';
import { PublicationsComponent } from './publications.component';
import {MaterialComponentsModule} from '../layout/material-components/material-components.module';


@NgModule({
  imports: [
    CommonModule,
    PublicationsRoutingModule,
    MaterialComponentsModule
  ],
  declarations: [PublicationsComponent]
})
export class PublicationsModule { }
