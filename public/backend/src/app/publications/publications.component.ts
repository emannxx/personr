import { Component, OnInit } from '@angular/core';
import { PublicationsService } from '../shared/services/publications.api.service';
import { Publication } from '../models/publication.model';
import { identifierName } from '@angular/compiler';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';


@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('void', style({ opacity: 0 })),
      state('*', style({ opacity: 1 })),
      transition(':enter', animate('500ms ease-out')),
      transition(':leave', [
        style({ /*transform: 'scale(1)', opacity: 1, height: '*' */}),
        animate('.5s cubic-bezier(.3, -0.6, 0.2, 1.5)', 
         style({ 
           transform: 'scale(1)', opacity: 0, 
           height: '0px', margin: '-15px' 
         })) 
      ]),
    ])
],
})

export class PublicationsComponent implements OnInit {
  
  constructor(private publicationService: PublicationsService) { }

  lstPublications:Publication[];
  contentLoaded:boolean = false;
  
  public splicer(id) {
    for (var i = 0; i < this.lstPublications.length; i++) {
      if (this.lstPublications[i].id === id) { 
        this.lstPublications.splice(i, 1);
        break;
      }
    }
  }

  public deletePublication(id:number) {
    console.log("deleting " + id);
    let elementPos;
    let objectFound;
    elementPos = this.lstPublications.map(function(x) {return x.id; }).indexOf(id);
    objectFound = this.lstPublications[elementPos];
    this.splicer(objectFound.id);
    this.publicationService.deletePublication(objectFound.id).subscribe(
      //@ts-ignore
      data => (alertify.success("Publication deleted"),console.log('Delete OK')),
      //@ts-ignore
      error => (emitNotification('An error occured!: ' + error.message),console.log(error))
    );
  }

  public fetchNextPage() {
    console.log(this.publicationService.nextPage);
    this.getPublications(this.publicationService.nextPage);
  }

  public getPublications(page?:string) {
    this.publicationService.getPublications(page).subscribe(
      data => (this.lstPublications = data.body['data'], this.contentLoaded = true),
      //@ts-ignore
      error => (emitNotification('An error occured!: ' + error.message),console.log(error))
    );
  }
  
 
  ngOnInit(){
    this.getPublications();

    /*var publication = {
      "id": 955,
      "title": "This is a test",
      "category_id": 1,
      "user_id": 1,
      "content": "The content of the test",
      "description": "Testing",
      "public_url": "abc"
    }

    this.publicationService.createPublication(publication).subscribe((res)=>{
      console.log("Created a Publication" + res);
    });*/
  }

}
