export class Publication {
    id: number;
    title: string;
    category_id: number;
    user_id: number;
    content: string;
    description: string;
    public_url: string;
    state:boolean;
    created_at?: string;
    updated_at?: string;
    deleted_at?: any;
}
