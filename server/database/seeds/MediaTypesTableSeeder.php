<?php

use Illuminate\Database\Seeder;

class MediaTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('prs_mediatypes')->insert([
            'type_name' => 'Picture',
        ]);

        DB::table('prs_mediatypes')->insert([
            'type_name' => 'Video',
        ]);

        DB::table('prs_mediatypes')->insert([
            'type_name' => 'Other',
        ]);
    }
}
