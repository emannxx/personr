<?php

use Illuminate\Database\Seeder;

class ProfileOperationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('prs_profile_operations')->insert([
            'profile_id' => 1,
            'operation_id' => 1,
        ]);

        DB::table('prs_profile_operations')->insert([
            'profile_id' => 2,
            'operation_id' => 1, //remove before deploy
        ]);
    }
}
