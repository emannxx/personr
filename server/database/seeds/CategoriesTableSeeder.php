<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prs_categories')->insert([
            'category_name' => "Base",
        ]);

        DB::table('prs_categories')->insert([
            'category_name' => "Uncategorised",
        ]);
    }
}
