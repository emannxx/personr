<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('prs_profiles')->insert([
            'id' => 1,
            'profile_name' => "Administrator",
        ]);

        DB::table('prs_profiles')->insert([
            'id' => 2,
            'profile_name' => "Guest",
        ]);
    }
}
