<?php

use Illuminate\Database\Seeder;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Media::class, 250)->create();
        factory(App\PublicationMedia::class, 120)->create();
    }
}
