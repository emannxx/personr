<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('prs_users')->insert([
            'name' => "Anonymous",
            'email' => "public@public.com",
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'state' => 1,
            'api_token' => 'GUEST',
            'profile_id' => 2,
            'picture' => "xxx"
        ]);
        factory(App\User::class, 4)->create();
    }
}
