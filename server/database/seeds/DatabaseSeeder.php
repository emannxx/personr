<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        $this->call('MediaTypesTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('OperationsTableSeeder');
        $this->call('ProfilesTableSeeder');
        $this->call('ProfileOperationsTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('PublicationsTableSeeder');
        $this->call('CommentsTableSeeder');
        $this->call('MediaTableSeeder');
    }
}
