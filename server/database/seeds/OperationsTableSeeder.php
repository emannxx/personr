<?php

use Illuminate\Database\Seeder;

class OperationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //GODMODE - SuperAdmin
        DB::table('prs_operations')->insert([
            'operation_name' => "GodMode",
            'operation_sigla' => "GOD",
            'concern' => 'APP',
        ]);

        //PUBLICATIONS
        DB::table('prs_operations')->insert([
            'operation_name' => "View Publications",
            'operation_sigla' => "view",
            'concern' => 'PUB',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Create Publications",
            'operation_sigla' => "create",
            'concern' => 'PUB',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Edit/Update Publications",
            'operation_sigla' => "update",
            'concern' => 'PUB',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Delete Publications",
            'operation_sigla' => "delete",
            'concern' => 'PUB',
        ]);


        //COMMENTS
        DB::table('prs_operations')->insert([
            'operation_name' => "View Comments",
            'operation_sigla' => "view",
            'concern' => 'COM',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Create Comments",
            'operation_sigla' => "create",
            'concern' => 'COM',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Edit/Update Comments",
            'operation_sigla' => "update",
            'concern' => 'COM',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Delete Comments",
            'operation_sigla' => "delete",
            'concern' => 'COM',
        ]);

        //CATEGORIES
        DB::table('prs_operations')->insert([
            'operation_name' => "View Categories",
            'operation_sigla' => "view",
            'concern' => 'CAT',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Create Categories",
            'operation_sigla' => "create",
            'concern' => 'CAT',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Edit/Update Categories",
            'operation_sigla' => "update",
            'concern' => 'CAT',
        ]);

        DB::table('prs_operations')->insert([
            'operation_name' => "Delete Categories",
            'operation_sigla' => "delete",
            'concern' => 'CAT',
        ]);
    }
}
