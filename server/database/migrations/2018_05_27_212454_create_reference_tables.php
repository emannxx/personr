<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prs_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('prs_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag_name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('prs_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('operation_name');
            $table->string('operation_sigla');
            $table->string('concern');
            $table->timestamps();
            $table->softDeletes();
            
            $table->index('concern');
            $table->index('operation_sigla');
        });

        Schema::create('prs_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile_name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('prs_mediatypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_name');
            $table->timestamps();
            $table->softDeletes();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prs_profiles');
        Schema::dropIfExists('prs_operations');
        Schema::dropIfExists('prs_categories');
        Schema::dropIfExists('prs_tags');
        Schema::dropIfExists('prs_mediatypes');
    }
}
