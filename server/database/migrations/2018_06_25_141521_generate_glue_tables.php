<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenerateGlueTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('prs_profile_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('profile_id');
            $table->unsignedInteger('operation_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('profile_id')->references('id')->on('prs_profiles');
            $table->foreign('operation_id')->references('id')->on('prs_operations');
        });

        Schema::create('prs_publication_media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('publication_id');
            $table->unsignedInteger('media_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('publication_id')->references('id')->on('prs_publications')->onDelete('cascade');
            $table->foreign('media_id')->references('id')->on('prs_media')->onDelete('cascade');
        });

        Schema::create('prs_publication_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('publication_id');
            $table->unsignedInteger('tag_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('publication_id')->references('id')->on('prs_publications');
            $table->foreign('tag_id')->references('id')->on('prs_tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('prs_profile_operations');
        Schema::dropIfExists('prs_publication_media');
        Schema::dropIfExists('prs_publication_tags');
    }
}
