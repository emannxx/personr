<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prs_users', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name', 140);
			$table->string('email',140)->unique();
			$table->string('password');
            $table->boolean('state');
            $table->string('api_token')->unique();
            $table->unsignedInteger('profile_id');
            $table->binary('picture');	
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('profile_id')->references('id')->on('prs_profiles');
        });

        Schema::create('prs_publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 140);
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id');
            $table->longText('content');
            $table->longText('description');
            $table->longText('public_url');
            $table->boolean('state');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('prs_users');
            $table->foreign('category_id')->references('id')->on('prs_categories');
            $table->index('user_id');	
            $table->index('category_id');
        });

        Schema::create('prs_media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mediatype_id');
            $table->binary('media_blob');
            $table->string('media_url');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mediatype_id')->references('id')->on('prs_mediatypes');
        });

        Schema::create('prs_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('publication_id');
            $table->unsignedInteger('user_id');
            $table->string('user_name');
            $table->longText('content');
            $table->boolean('is_visible');
            $table->boolean('state');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('prs_users')->onDelete('cascade');
            $table->foreign('publication_id')->references('id')->on('prs_publications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prs_comments');        
        Schema::dropIfExists('prs_publications');
        Schema::dropIfExists('prs_users');
        Schema::dropIfExists('prs_media');
    }
}
