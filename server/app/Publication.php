<?php namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publication extends Model {
    use SoftDeletes;

    protected $table = 'prs_publications';

    protected $fillable = ["title","category_id","content","description","state"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public static function fromUser($iduser = null) {
        if (is_null($iduser)) {
            return null;
        }
        else {
            return Publication::where('user_id', $iduser)->first();
        }
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function comments() {
        return $this->hasMany('App\Comment', 'publication_id');
    }

    public function media() {
        return $this->hasManyThrough(
            'App\Media', 
            'App\PublicationMedia', 
            'publication_id', 
            'id', 
            'publication_id', 
            'media_id' 
        );
    }
}
