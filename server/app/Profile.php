<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Profile extends Model {
    use SoftDeletes;

    protected $table = 'prs_profiles';

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];


}
