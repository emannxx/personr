<?php namespace App\Http\Controllers;
use App\Publication;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
class CategoriesController extends Controller {

    const MODEL = "App\Category";

    use RESTActions;

    public function getCategoryPublications(Request $request, $id) {
        if (($request->user()->can('view', new Publication)) && ($request->user()->can('view', new Category))) {
            $cat = Category::find($id);
            if ($cat && !is_null($cat) && sizeof($cat)>0 && $cat->id > 0) {
                return $this->respond(Response::HTTP_OK,$cat->publications);
            }
            else {
                return $this->respond(Response::HTTP_NOT_FOUND);
            }
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }

}
