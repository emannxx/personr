<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait RESTActions {


    public function all()
    {
        $m = self::MODEL;
        return $this->respond(Response::HTTP_OK, $m::paginate(10));
    }

    public function get(Request $request, $id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        if ($request->user()->can('view', $model)) {
            if(is_null($model)){
                return $this->respond(Response::HTTP_NOT_FOUND);
            }
            return $this->respond(Response::HTTP_OK, $model);
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }

    public function add(Request $request)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        if ($request->user()->can('create', $m)) {
            return $this->respond(Response::HTTP_CREATED, $m::create($request->all()));
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }

    public function put(Request $request, $id)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        else {
            if ($request->user()->can('update', $model)) {
                $model->update($request->all());
                return $this->respond(Response::HTTP_OK, $model);
            }
            else {
                return $this->respond(Response::HTTP_UNAUTHORIZED);
            }
        }
        
    }

    public function remove(Request $request, $id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        if ($request->user()->can('delete', $model)) {
            $model->delete($id); // TODO: DEVELOP CUSTOM FUNCTION
            return $this->respond(Response::HTTP_OK);
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
        
    }

    protected function respond($status, $data = [])
    {
        return response()->json($data, $status);
    }

}
