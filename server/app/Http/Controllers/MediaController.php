<?php namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use App\Publication;
use App\User;
use App\Media;
use Illuminate\Http\Request;

class MediaController extends Controller {

    const MODEL = "App\Media";

    use RESTActions;

    public function getPublicationMedia(Request $request, $id) {
        if ($request->user()->can('view', new Publication)) {
            $pub = Publication::find($id);
            if ($pub && !is_null($pub) && sizeof($pub)>0 && $pub->id > 0) {
                return $this->respond(Response::HTTP_OK,$pub->media);
            }
            else {
                return $this->respond(Response::HTTP_NOT_FOUND);
            }
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }

}
