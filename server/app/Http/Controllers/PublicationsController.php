<?php namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use App\Publication;
use App\User;
use App\Comment;
use Illuminate\Http\Request;


class PublicationsController extends Controller {
    const MODEL = "App\Publication";
    use RESTActions;

    public function postPublication(Request $request) {
        if ($request->user()->can('create', new Publication)) {
            $pub = new Publication;
            $pub->fill($request->all());        
            $pub['user_id'] = $request->user()->id;
            $pub->save();
            return $this->respond(Response::HTTP_OK, $pub->id);         
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }

    }

    public function deletePublication(Request $request, $id) {
        if ($request->user()->can('delete', new Publication)) {
            $pub = Publication::where('id', $id)->first();
            if (is_null($pub)) {
                return $this->respond(Response::HTTP_NOT_FOUND);
            }
            else {
                if ($pub->comments()->count() > 0) {
                    $pub->comments()->delete();
                }
                if ($pub->media()->count() > 0) {
                    $pub->media()->delete();
                }                
                $pub->delete();
                return $this->respond(Response::HTTP_OK); 
            }
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }
    public function getUserPublications(Request $request, $id)
    {
        if ($request->user()->can('view', new Publication)) {
            $usr = User::find($id);
            if ($usr && !is_null($usr)) {
                return $this->respond(Response::HTTP_OK,$usr->first()->publications);
            }
            else {
                return $this->respond(Response::HTTP_NOT_FOUND);
            }
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }

    public function getPublicationComments(Request $request, $id) {
        if (($request->user()->can('view', new Publication)) && ($request->user()->can('view', new Comment))) {
            $pub = Publication::find($id);
            if ($pub && !is_null($pub)) {
                return $this->respond(Response::HTTP_OK, $pub->first()->comments);
            }
            else {
                return $this->respond(Response::HTTP_NOT_FOUND);
            }
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
    }

}
