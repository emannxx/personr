<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;
use App\Comment;

use App\User;

class UsersController extends Controller {

    const MODEL = "App\User";

    use RESTActions;

    public function postRegisterUser(Request $request) {
        if ($request->input('name') == "" || $request->input('email') == "" || $request->input('password')== "") {
            return $this->respond(Response::HTTP_BAD_REQUEST, 'Empty name, email or password.');
        }
        else {
            if (User::where('email', $request->input('email'))->first() != null) {
                return $this->respond(Response::HTTP_CONFLICT,'Email already used.');
            }
            else {
                $user = new User;
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->password = Hash::make($request->input('password'));
                $user->state = 1;
                $user->api_token = uniqid();
                $user->profile_id = 2;
                $user->save();
                return $this->respond(Response::HTTP_OK, 'User created, you may log-in now.');
            }
        }
    }

    public function getUserPermissions(Request $request, $id) {
        if ($id == "") {
            return $this->respond(Response::HTTP_BAD_REQUEST, "Invalid ID");
        }
        else {
            $isGod = $request->user()->permissions()->get()->contains('operation_sigla','GOD');
            
            if (($request->user()->id == $id) || $isGod) {

                $user = User::where('id', $id)->first();
                if ($user != null) {
                    $user_permissions = $user->permissions()->get();
                    if (sizeof($user_permissions) > 0) {
                        return $this->respond(Response::HTTP_OK, $user_permissions);
                    }
                    else {
                        return $this->respond(Response::HTTP_NO_CONTENT);
                    }
                }
                else {
                    return $this->respond(Response::HTTP_NOT_FOUND);
                }
            }
            else {
                return $this->respond(Response::HTTP_UNAUTHORIZED);
            }
        }
    }

    public function postLogin(Request $request) {
        if ($request->input('email') == "" || $request->input('password') == "") {
            return $this->respond(Response::HTTP_BAD_REQUEST, "Empty 'email' or 'password' params.");
        }
        else {
            $user = User::where('email', $request->input('email'))->first();
            if ($user === null) {
                return $this->respond(Response::HTTP_NOT_FOUND, "Couldn't find the specified user.");
            }
            else {
                $tokenId = bin2hex(random_bytes(160));
                $tokenId = base64_encode(substr($tokenId,0,32));
                $issuedAt   = time();
                $notBefore  = $issuedAt;  
                $expire     = $notBefore + 7200; 
                $serverName = 'http://localhost'; /// set your domain name 

                $data = [
                    'iat'  => $issuedAt,         // Issued at: time when the token was generated
                    'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                    'iss'  => $serverName,       // Issuer
                    'nbf'  => $notBefore,        // Not before
                    'exp'  => $expire,           // Expire
                    'data' => [                  // Data related to the logged user you can set your required data
                        'id'   => $user->id, // id from the users table
                        'name' => $user->name, //  name
                        'email' => $user->email, //  email
                    ]
                ];

                $jwt = JWT::encode($data, env('AUTH_JWTKEY', null), env('AUTH_ALGO', null));
                return $this->respond(Response::HTTP_OK, ["token" => $jwt, "id" => $user->id, "name" => $user->name]);
            }
        }
    }

    public function getUserComments(Request $request, $id) {
        if ($request->user()->can('view', new Comment)) {
            $user = User::find($id);
            if ($user && !is_null($user) && sizeof($user)>0) {
                return $this->respond(Response::HTTP_OK, $user->comments);
            }
            else {
                return $this->respond(Response::HTTP_NOT_FOUND);
            } 
        }
        else {
            return $this->respond(Response::HTTP_UNAUTHORIZED);
        }
        
    }
    
}
