<?php

namespace App\Providers;

use App\User;
use App\Publication;
use App\Comment;
use App\Category;


use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Policies\PublicationPolicy;
use App\Policies\UserPolicy;
use App\Policies\CommentPolicy;
use App\Policies\CategoryPolicy;
use Firebase\JWT\JWT;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

          
    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        Gate::policy(Publication::class, PublicationPolicy::class);
        Gate::policy(User::class, UserPolicy::class);
        Gate::policy(Comment::class, CommentPolicy::class);
        Gate::policy(Category::class, CategoryPolicy::class);
                
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->header('Authorization') && $request->header('Authorization') != null) {
                try {
                    $decoded_array = (array) JWT::decode($request->header('Authorization'), env('AUTH_JWTKEY', null), array(env('AUTH_ALGO', null)));
                    $user = User::find($decoded_array['data']->id);
                    if ($decoded_array['data']->name === $user->name && $decoded_array['data']->email === $user->email) {
                        return $user;
                    }
                    else {
                        throw new \Exception('Invalid login');
                    }
                }
                catch (\Exception $e) {
                    return User::where('email', 'public@public.com')->first();
                }
            }
            else {
                return User::where('email', 'public@public.com')->first();
            }
        });
    }
}
