<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model {
    use SoftDeletes;
    protected $table = 'prs_categories';

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function publications() {
        return $this->hasMany('App\Publication');
    }
}
