<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model {
    use SoftDeletes;
    protected $table = 'prs_comments';

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public function publication() {
        return $this->belongsTo('App\Publication');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
    // Relationships

}
