<?php
namespace App\Policies;
 
use App\User;
use App\Category;
use App\ProfileOperation;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy extends BasePolicy
{
 
  use HandlesAuthorization;

  public function __construct() {
    $this->concern = 'CAT'; //defines BasePolicy->concern
  }

  /**
   * Determine whether the user can view the Category.
   */
  public function view($user, $cat)
  {
    return $this->checkpermission('view'); // BasePolicy -> checkpermission();
  }
 
  /**
   * Determine whether the user can create Categorys.
   */
  public function create(User $user)
  {
    if ($this->checkpermission('create')) {
      return $user->id > 0;
    }
    else {
      return false;
    }
    
  }
 
  /**
   * Determine whether the user can update the Category.
   */
  public function update(User $user)
  {
    if ($this->checkpermission('update')) {
      return true;
    }
    else {
      return false;
    }
  }
 
  /**
   * Determine whether the user can delete the Category.
   */
  public function delete(User $user)
  {
    if ($this->checkpermission('delete')) {
      return true;
    }
    else {
      return false;
    }
  }
}