<?php
namespace App\Policies;
 
use App\User;
use App\ProfileOperation;
use Illuminate\Auth\Access\HandlesAuthorization;
 
class UserPolicy extends BasePolicy
{
  use HandlesAuthorization;
 
  public function __construct() {
    $this->concern = 'USR'; //defines BasePolicy->concern
  }
  /**
   * Determine whether the user can view the Publication.
   *
   * @param  \App\User  $user
   * @param  \App\User  $userb
   * @return mixed
   */
  public function view($user, $userb)
  {
    /*if ($user->id === $userb->id) {
        return true;
    }
    else {
        return false;
    }*/
    if ($this->checkpermission('view')) {
      if ($this->checkGod()) {
        return true;
      }
      else {
        return $user->id === $userb->id;
      }
    }
    else {
      return false;
    }
  }
 
  /**
   * Determine whether the user can create Publications.
   *
   * @param  \App\User  $user
   * @return mixed
   */
  public function create(User $user)
  {
    return $this->checkpermission('create');
    //return $user->id > 0;
  }
 
  /**
   * Determine whether the user can update the Publication.
   *
   * @param  \App\User  $user
   * @param  \App\Publication  $pub
   * @return mixed
   */
  public function update(User $user, User $userb)
  {
    if ($this->checkpermission('update')) {
      return $user->id == $userb->id;
    }
    else {
      return false;
    }
    
  }
 
  /**
   * Determine whether the user can delete the Publication.
   *
   * @param  \App\User  $user
   * @param  \App\Publication  $pub
   * @return mixed
   */
  public function delete(User $user, User $userb)
  {
    if ($this->checkpermission('delete')) {
      return $user->id == $userb->id;
    }
    else {
      return false;
    }
  }
}