<?php
namespace App\Policies;
use App\User;
use App\Comment;
use App\ProfileOperation;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy extends BasePolicy
{
 
  use HandlesAuthorization;

  public function __construct() {
    $this->concern = 'COM'; //defines BasePolicy->concern
  }

  /**
   * Determine whether the user can view the Comment.
   */
  public function view(User $user, Comment $com)
  {
    return $this->checkpermission('view');
  }
 
  /**
   * Determine whether the user can create Comments.
   */
  public function create(User $user)
  {
    if ($this->checkpermission('create')) {
      return $user->id > 0;
    }
    else {
      return false;
    }
    
  }
 
  /**
   * Determine whether the user can update the Comment.
   */
  public function update(User $user, Comment $com)
  {
    if ($this->checkpermission('update')) {
      return $this->checkGod() ? true : $user->id == $com->user_id;
    }
    else {
      return false;
    }
  }
 
  /**
   * Determine whether the user can delete the Comment.
   */
  public function delete(User $user, Comment $com)
  {
    if ($this->checkpermission('delete')) {
      return $this->checkGod() ? true : $user->id == $com->user_id;
    }
    else {
      return false;
    }
  }
}
?>