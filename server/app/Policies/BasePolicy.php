<?php
namespace App\Policies;
use Log;
use App\User;
use App\Publication;
use App\ProfileOperation;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy {
    protected $user_permissions;
    protected $concern;
    private $isGod;

    public function before($user, $ability) {
        $this->user_permissions = $user->permissions()->get();  // fetch all permissions of said user
        /**
         * God has special powers. God is omnipotent (eg. who has unlimited power) and omnipresent (eg. who is widely or constantly encountered; widespread.).
         * God is super
         * God is the super user
         */
        if ($this->user_permissions->contains('operation_sigla','GOD')) {
            //This is God; Atheists might disagree.
            $this->isGod = true; 
        }
        else {
            $this->isGod = false;
        }
    }

    protected function checkpermission($op) {
        if ($this->checkGod()) {
            return true;
        }
        else {
            if ($this->user_permissions->where('concern',$this->concern)->contains('operation_sigla',$op)) {
                return true;
            } 
            else {
                return false;
            }
        }
    }

    public function checkGod() {
        if (!is_null($this->isGod)) {
            return $this->isGod;
        }
        else {
            return false;
        }
    }
}
?>