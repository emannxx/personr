<?php
namespace App\Policies;
 
use App\User;
use App\Publication;
use App\ProfileOperation;
use Illuminate\Auth\Access\HandlesAuthorization;

class PublicationPolicy extends BasePolicy
{
 
  use HandlesAuthorization;

  public function __construct() {
    $this->concern = 'PUB'; //defines BasePolicy->concern
  }

  /**
   * Determine whether the user can view the Publication.
   */
  public function view($user, $pub)
  {
    return $this->checkpermission('view'); // BasePolicy -> checkpermission();
  }
 
  /**
   * Determine whether the user can create Publications.
   */
  public function create(User $user)
  {
    if ($this->checkpermission('create')) {
      return $user->id > 0;
    }
    else {
      return false;
    }
    
  }
 
  /**
   * Determine whether the user can update the Publication.
   */
  public function update(User $user, Publication $pub)
  {
    if ($this->checkpermission('update')) {
      return $this->checkGod() ? true : $user->id == $pub->user_id;
    }
    else {
      return false;
    }
  }
 
  /**
   * Determine whether the user can delete the Publication.
   */
  public function delete(User $user, Publication $pub)
  {
    if ($this->checkpermission('delete')) {
      return $this->checkGod() ? true : $user->id == $pub->user_id;
    }
    else {
      return false;
    }
  }
}