<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    protected $table = 'prs_users';
    protected $dates = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id');
    }

    public function permissions() {
        return $this->hasManyThrough(
            'App\Operation', 
            'App\ProfileOperation', 
            'profile_id', 
            'id', 
            'profile_id', 
            'operation_id' 
        );
    }

    public function publications() {
        return $this->hasMany('App\Publication');
    }

    public function comments() {
        return $this->hasMany('App\Comment', 'user_id');
    }
    
}