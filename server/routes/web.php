<?php
use Illuminate\Http\Request;
use Illuminate\Http\Response;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return;
});


//AUTHENTICATED
$router->group(['middleware' => 'auth'], function () use ($router) {
    /**
    * Routes for resource user
    */
    $router->get('user/{id}', 'UsersController@get');
    $router->post('user', 'UsersController@add');
    $router->put('user/{id}', 'UsersController@put');
    $router->delete('user/{id}', 'UsersController@remove');
    $router->get('user/{id}/comments', 'CommentsController@getUserComments');
    $router->get('user/{id}/permissions', 'UsersController@getUserPermissions');

    /**
    * Routes for resource publication
    */
    $router->get('publications', 'PublicationsController@all');
    $router->post('publications', 'PublicationsController@postPublication');
    $router->put('publications/{id}', 'PublicationsController@put');
    $router->delete('publications/{id}', 'PublicationsController@deletePublication');
    $router->get('user/{id}/publications', 'PublicationsController@getUserPublications');

    /**
    * Routes for resource comment
    */
    $router->get('comments', 'CommentsController@all');
    $router->get('comments/{id}', 'CommentsController@get');
    $router->post('comments', 'CommentsController@add');
    $router->put('comments/{id}', 'CommentsController@put');
    $router->delete('comments/{id}', 'CommentsController@remove');

    /**
    * Routes for resource category
    */
    $router->post('categories', 'CategoriesController@add');
    $router->put('categories/{id}', 'CategoriesController@put');
    $router->delete('categories/{id}', 'CategoriesController@remove');

    /**
     * Routes for resource Media
     */
    $router->get('media', 'MediaController@all');
    $router->post('media', 'MediaController@add');
    $router->put('media/{id}', 'MediaController@put');
    $router->delete('media/{id}', 'MediaController@remove');
});

//PUBLIC

$router->get('categories', 'CategoriesController@all');
$router->get('categories/{id}', 'CategoriesController@get');
$router->get('categories/{id}/publications', 'CategoriesController@getCategoryPublications');
$router->get('publications/{id}', 'PublicationsController@get');
$router->get('publications/{id}/comments', 'PublicationsController@getPublicationComments');
$router->get('publications/{id}/media', 'MediaController@getPublicationMedia');


$router->post('user/register', 'UsersController@postRegisterUser');
$router->post('user/login', 'UsersController@postLogin');

/**
 * Public Routes for resource media
 */

$router->get('media/{id}', 'MediaController@get');
$router->get('media/{id}', 'MediaController@get');




